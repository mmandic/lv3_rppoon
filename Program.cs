using System;
using System.Collections.Generic;

namespace LV3_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Zad1");
            Console.WriteLine("Prvi");
            Prototype m1 = new Dataset(@"C:\csv.csv");
            Debug((Dataset)m1);
            Console.WriteLine("Duboko Kopiranje");
            Prototype m2 = m1.Clone();
            Debug((Dataset)m2);
            Console.WriteLine("__________\n");
            /* Dataset nema slozene tipove podataka tako da duboko koipiranje nije potrebno*/
            
            Console.WriteLine("Zad2");
            Console.WriteLine("matrica 2x3");
            MatrixGenerator m = MatrixGenerator.GetInstance();
            float[][] M = m.CreateMatrix(2, 3);
            Debug(M);
            Console.WriteLine("________\n");
            
            Console.WriteLine("Zad3");
            Console.WriteLine("U D:log.txt je recenica sa dvije funkcije.");
            Logger logger = Logger.GetInstance();
            logger.SetFilepath(@"D:\log.txt");
            Console.WriteLine("\n____________\n");
            //Logger je singleton pa pišemo obje funkcije na isto mjesto
            
            Console.WriteLine("Zad4");
            Console.WriteLine("Testiranje sa podatcima");
            ConsoleNotification notif = new ConsoleNotification("Anonymous", "Notification", "NotificationText", DateTime.Now, Category.ERROR, ConsoleColor.Red);
            NotificationManager manager = new NotificationManager();
            manager.Display(notif);
            Console.WriteLine("\n_______________\n");
            
            Console.WriteLine("Zad 5");
            Console.WriteLine("Testiranja");
            NotificationBuilder builder = new NotificationBuilder();
            builder.SetAuthor("Anonymous");
            builder.SetTitle("Notification");
            builder.SetText("NotificationText");
            builder.SetTime(DateTime.Now);
            builder.SetLevel(Category.ERROR);
            builder.SetColor(ConsoleColor.Red);
            NotificationManager manager = new NotificationManager();
            manager.Display(builder.Build());
            Console.WriteLine("\n____________________\n");
        }
    }
}
