using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON
{
    class MatrixGenerator
    {
        public static MatrixGenerator instance;
        private Random random;

        private MatrixGenerator()
        {
            random = new Random();
        }

        public static MatrixGenerator GetInstance()
        {
            if(instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }

        //Zadatak 2
        
        
        public float[][] CreateMatrix(int width, int height)
        {
            float[][]M = new float[height][];
            for(int i = 0; i < M.Length; i++)
            {
                M[i] = new float[width];
            }

            for(int i = 0; i < height; i++)
            {
                for(int j= 0; j < width; j++)
                {
                    M[i][j] = (float)random.NextDouble();
                }
            }

            return M;
           
        }
    }
}
